


#include "./includes/solver.hpp"
#include "./src/solver.cc"
#include "./includes/multiGrid.hpp"
#include "./src/multiGrid.cc"
#include "./includes/vCycle.hpp"
#include "./src/vCycle.cc"
#include "./includes/fullMultiGrid.hpp"
#include "./src/fullMultiGrid.cc"
#include "./includes/residual.hpp"
#include "./src/residual.cc"
#include "./includes/restriction.hpp"
#include "./src/restriction.cc"
#include "./includes/relaxation.hpp"
#include "./src/relaxation.cc"
#include "./includes/prolongation.hpp"
#include "./src/prolongation.cc"
#include "./includes/boundaryCondition.hpp"
#include "./src/boundaryCondition.cc"
#include "./includes/rhsFunctionDict.hpp"
#include "./src/rhsFunctionDict.cc"
#include "./includes/gnuplot.hpp"
#include "./src/gnuplot.cc"

#ifdef LOCAL_CL
#include "./includes/CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif
#include <fstream>

int main()
{
   int dim;
   std::cout << "dim = ";
   std::cin >> dim;
   solver<float> mySolver(dim, dim, dim);
   multiGrid<float> mg(mySolver, "GS", 1.15, CL_DEVICE_TYPE_GPU, 3);
   vCycle<float> vcycle(mg, 1, 1, false);
   vcycle.solve();
//   fullMultiGrid<float> fmg(mg, 2, 1, false);
//   fmg.solve();
}

