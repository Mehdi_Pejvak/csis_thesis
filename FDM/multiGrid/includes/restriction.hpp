
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/

#ifndef RESTRICTION_HPP
#define RESTRICTION_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <iomanip>
#include "CL_ERROR.hpp"
#include "defines.hpp"
#include <chrono>

template<class T>
class restriction
{
  protected:
    int restrictionStencilWidth = 3;
    int restrictionStencilSize = 27;
    T* stencil     = NULL;
    cl::Kernel       restrictionKernel;
    cl::Kernel       additionKernel;
    cl::Buffer       restrictionStencilBuffer;

    long double timeOfOperation = 0.0;
  public:
     restriction(cl::Program& _program, cl::CommandQueue& _queue,
                 cl::Context& _context);

    ~restriction();

     const long double getTime() const;

     void setStencil(cl::Context& context, cl::CommandQueue& queue);

     void createKernel(cl::Program& program);

     void doRestriction(cl::CommandQueue& queue, 
                    cl::Buffer& inputBuffer, cl::Buffer& outputBuffer,
                    const cl::NDRange& srcGlobalRange, const cl::NDRange& srcLocalRange,
                    const int& srcDeviceHeight, const int& srcDeviceWidth,
                    const int& srcDeviceDepth, const int& distDeviceWidth,
                    const std::vector<int> localSize, int subt, cl::Event& event);
};


#endif














