
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/


#include "../includes/restriction.hpp"

template<class T>
restriction<T>::restriction(cl::Program& _program, cl::CommandQueue& _queue,
                            cl::Context& _context)
{
  setStencil(_context, _queue);
  createKernel(_program);
}


template<class T>
restriction<T>::~restriction()
{
   delete[] stencil;
}

template<class T>
inline const long double restriction<T>::getTime() const
{
   return timeOfOperation;
}


template<class T>
void restriction<T>::setStencil(cl::Context& context, cl::CommandQueue& queue)
{
   stencil = new T[restrictionStencilSize];

  stencil[0]  = 1.0f / 64.0f;
  stencil[1]  = 2.0f / 64.0f;
  stencil[2]  = 1.0f / 64.0f;
  stencil[3]  = 2.0f / 64.0f;
  stencil[4]  = 4.0f / 64.0f;
  stencil[5]  = 2.0f / 64.0f;
  stencil[6]  = 1.0f / 64.0f;
  stencil[7]  = 2.0f / 64.0f;
  stencil[8]  = 1.0f / 64.0f;
  
  stencil[9]  = 2.0f / 64.0f;
  stencil[10] = 4.0f / 64.0f;
  stencil[11] = 2.0f / 64.0f;
  stencil[12] = 4.0f / 64.0f;
  stencil[13] = 8.0f / 64.0f;
  stencil[14] = 4.0f / 64.0f;
  stencil[15] = 2.0f / 64.0f;
  stencil[16] = 4.0f / 64.0f;
  stencil[17] = 2.0f / 64.0f;
  
  stencil[18] = 1.0f / 64.0f;
  stencil[19] = 2.0f / 64.0f;
  stencil[20] = 1.0f / 64.0f;
  stencil[21] = 2.0f / 64.0f;
  stencil[22] = 4.0f / 64.0f;
  stencil[23] = 2.0f / 64.0f;
  stencil[24] = 1.0f / 64.0f;
  stencil[25] = 2.0f / 64.0f;
  stencil[26] = 1.0f / 64.0f;


  try
  {
    restrictionStencilBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, 
                                           restrictionStencilSize * sizeof(T));
     queue.enqueueWriteBuffer(restrictionStencilBuffer,  CL_TRUE, 0, 
                              restrictionStencilSize * sizeof(T), stencil);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in buffer creation/writing data to "
                 "device " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T> 
void restriction<T>::createKernel(cl::Program& program)
{
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Restriction class, Creating kernels";
  try
  {
    restrictionKernel  = cl::Kernel(program, "restriction");
    std::cout << std::right << "  -> Done! " << std::endl;
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in kernel  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T>
void restriction<T>::doRestriction(cl::CommandQueue&  queue, 
                   cl::Buffer& inputBuffer, cl::Buffer& outputBuffer,
                   const cl::NDRange& srcGlobalRange, const cl::NDRange& srcLocalRange,
                   const int& srcDeviceHeight, const int& srcDeviceWidth,       
                   const int& srcDeviceDepth, const int& distDeviceWidth, 
                   const std::vector<int> localSize, int subt, cl::Event& event)
{

  auto t1 = std::chrono::high_resolution_clock::now();
  int localMemSize = localSize[0] * localSize[1] * localSize[2] * sizeof(T);
  int argCount = 0; 
  try
  {
    restrictionKernel.setArg(argCount++, inputBuffer);
    restrictionKernel.setArg(argCount++, srcDeviceHeight);
    restrictionKernel.setArg(argCount++, srcDeviceWidth);
    restrictionKernel.setArg(argCount++, srcDeviceDepth);
    restrictionKernel.setArg(argCount++, restrictionStencilBuffer);
    restrictionKernel.setArg(argCount++, restrictionStencilWidth);
    restrictionKernel.setArg(argCount++, outputBuffer);
    restrictionKernel.setArg(argCount++, distDeviceWidth);
    restrictionKernel.setArg(argCount++, subt);
    restrictionKernel.setArg(argCount++, localMemSize, NULL);
    restrictionKernel.setArg(argCount++, localSize[0]);
    restrictionKernel.setArg(argCount++, localSize[1]);
    restrictionKernel.setArg(argCount++, localSize[2]);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in setting the argument of kernel "
                      << "restriction" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(restrictionKernel, cl::NullRange, 
                               srcGlobalRange, srcLocalRange, NULL, &event);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in enqueue kernel restrcition" << 
                 std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in finishing enqueue" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  auto t2 = std::chrono::high_resolution_clock::now();
  timeOfOperation +=  std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count() ;

}
