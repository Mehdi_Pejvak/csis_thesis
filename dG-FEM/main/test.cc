


#include "../multiGrid/includes/solver.hpp"
#include "../multiGrid/src/solver.cc"
#include "../multiGrid/includes/multiGrid.hpp"
#include "../multiGrid/src/multiGrid.cc"
#include "../multiGrid/includes/vCycle.hpp"
#include "../multiGrid/src/vCycle.cc"
#include "../multiGrid/includes/fullMultiGrid.hpp"
#include "../multiGrid/src/fullMultiGrid.cc"
#include "../multiGrid/includes/residual.hpp"
#include "../multiGrid/src/residual.cc"
#include "../multiGrid/includes/restriction.hpp"
#include "../multiGrid/src/restriction.cc"
#include "../multiGrid/includes/relaxation.hpp"
#include "../multiGrid/src/relaxation.cc"
#include "../multiGrid/includes/prolongation.hpp"
#include "../multiGrid/src/prolongation.cc"
#include "../multiGrid/includes/boundaryCondition.hpp"
#include "../multiGrid/src/boundaryCondition.cc"
#include "../multiGrid/includes/rhsFunctionDict.hpp"
#include "../multiGrid/src/rhsFunctionDict.cc"
#include "../multiGrid/includes/gnuplot.hpp"
#include "../multiGrid/src/gnuplot.cc"
#include "../dG/includes/dGLocalMatrix.hpp"
#include "../dG/src/dGLocalMatrix.cc"
#include "../dG/includes/dGLGLLocalMatrix.hpp"
#include "../dG/src/dGLGLLocalMatrix.cc"
#include "../dG/includes/dGProlStencilCreator.hpp"
#include "../dG/src/dGProlStencilCreator.cc"

#include "/home/mehdi/library/boost/boost/math/special_functions/legendre.hpp"

#ifdef LOCAL_CL
#include "../multiGrid/includes/CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif
#include <fstream>

int main()
{
   int dim;
   std::cout << "dim = ";
   std::cin >> dim;

   int nu_1 = 1;
   int nu_2 = 1;

   double omega = 1.0;// 0.857;

   int polynomialOrder = 3;
   std::string method = "SIP";
   double penalty = 10.0;

   solver<float> mySolver(dim, dim, dim, polynomialOrder, method, penalty);
   multiGrid<float> mg(mySolver, "jacobi", omega, CL_DEVICE_TYPE_GPU, 1, 
                       polynomialOrder, method, penalty);

   vCycle<float> vcycle(mg, nu_1, nu_2, false);
   vcycle.solve();

//   fullMultiGrid<float> fmg(mg, 2, 1, false);
//   fmg.solve();
}

