/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 03.05.2020
\*-----------------------------------------------------------------------*/


#include "../includes/multiGrid.hpp"

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template <class T>
multiGrid<T>::multiGrid( T* _RHS,
            int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
           std::string _relaxationMethod, T _omega, 
           cl_device_type _deviceType, int _deviceID,
            int _polynomialOrder,  std::string _method,  double _penalty)
           :
           solver<T>(_RHS, _origCellNo_x, _origCellNo_y, _origCellNo_z),
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}


template <class T>
multiGrid<T>::multiGrid(std::ifstream _RHSFILENAME,
            int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
           std::string _relaxationMethod, T _omega, 
           cl_device_type _deviceType, int _deviceID,
            int _polynomialOrder,  std::string _method,  double _penalty)
           :
           solver<T>(_RHSFILENAME, _origCellNo_x, _origCellNo_y, _origCellNo_z, 
                     _polynomialOrder, _method, _penalty),
           relaxationMethod{_relaxationMethod},
           omega{_omega},
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}



template <class T>
multiGrid<T>::multiGrid( int _origCellNo_x,  int _origCellNo_y, 
            int _origCellNo_z,
           std::string _relaxationMethod, T _omega, 
           cl_device_type _deviceType, int _deviceID,
            int _polynomialOrder,  std::string _method,  double _penalty)
           :
           solver<T>(_origCellNo_x, _origCellNo_y, _origCellNo_z, 
                     _polynomialOrder, _method, _penalty),
           relaxationMethod{_relaxationMethod},
           omega{_omega},
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}


template <class T>
multiGrid<T>::multiGrid(solver<T>& _mgSolver,
           std::string _relaxationMethod, T _omega, 
           cl_device_type _deviceType, int _deviceID,
            int _polynomialOrder,  std::string _method,  double _penalty)
           :
           solver<T>(_mgSolver, 
                     _polynomialOrder, _method, _penalty),
           relaxationMethod{_relaxationMethod},
           omega{_omega},
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}


template <class T>
multiGrid<T>::multiGrid(std::string _relaxationMethod, T _omega, 
                        cl_device_type _deviceType, int _deviceID)
           :
           solver<T>(),
           relaxationMethod{_relaxationMethod},
           omega{_omega},
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

template <class T>
multiGrid<T>::~multiGrid()
{
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template <class T>
void multiGrid<T>::setDevice()
{
  cl::Platform::get(&platforms);

  assert(platforms.size() > 0);

  myPlatform = cl::Platform(platforms[0]);

  myPlatform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
  assert(devices.size() > 0);

  device = devices[deviceID];
  displayDeviceInfo();
  context = cl::Context(device);
}


template <class T>
void multiGrid<T>::displayDeviceInfo()
{
  std::cout << "\n____________________DEVICE INFO_______________________\n" << std::endl;
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Platform"; 
  std::cout << std::right << ":  " << myPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Device"; 
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

  std::vector<::size_t> maxWorkItems;
  maxWorkItems = device.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Max. Work Item Sizes";
  std::cout << std::right << ":  " << maxWorkItems[0] << "*" << maxWorkItems[1] 
            << "*" << maxWorkItems[2] << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Max. Work Group Size";
  std::cout << std::right <<  ":  " << device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() 
            << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Global Memory Size";
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>()
                             / 1024/1024/1024 << " GB" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Local Memory Size";
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>()
                           / 1024 << " KB" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Constant Memory Size";
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>() 
                            / 1024 << " KB" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Number of Compute Unit"; 
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() 
            << std::endl;

  std::cout << "______________________________________________________\n" << std::endl;
}


template <class T>
int multiGrid<T>::roundUp(unsigned int value, unsigned int multiple)
{
  int remainder = value % multiple;

  if(remainder != 0)
  {
    value += (multiple - remainder);
  }
  return value;
}


template <class T>
inline void multiGrid<T>::setNumberOfMGLevels()
{
  multiGridLevels = (int) std::min(log2((double)this->origCellNo_y), 
                          std::min(log2((double)this->origCellNo_x), 
                                   log2((double)this->origCellNo_z))) - 1;
}


template <class T>
void multiGrid<T>::buildProgram(T omega)
{
  setDevice();

  std::ifstream kernelFile(kernelFileName);

  std::string src(std::istreambuf_iterator<char>(kernelFile), 
                 (std::istreambuf_iterator<char>()));

  cl::Program::Sources sources;
  try
  { 
     sources =  cl::Program::Sources(1,std::make_pair(src.c_str(),src.length() + 1));
  }catch (const cl::Error& error)
  {
     std::cout << "  -> Multigrid Class, Problem in creating source  " << std::endl;
     std::cout << "  -> " << getErrorString(error) << std::endl;
     exit(0);
  }
  
  try
  {
     program = cl::Program(context, sources);
  }catch (const cl::Error& error)
  {
     std::cout << "  -> Multigrid Class, Problem in creating program  " << std::endl;
     std::cout << "  -> " << getErrorString(error) << std::endl;
     exit(0);
  }
  
  // create queue to which we will push commands for the device.
  try
  {
     queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);
  }catch (const cl::Error& error)
  {
     std::cout << "  -> Multigrid Class, Problem in creating queue  " << std::endl;
     std::cout << "  -> " << getErrorString(error) << std::endl;
     exit(0);
  }

  // create preprocessor defines for the kernel
  std::string buildOptions;
  {
     // find type of template (T) and pass it as preprocessor to kernel
     int status;
     std::string tname = typeid(T).name();
     char *typeOfData = abi::__cxa_demangle(tname.c_str(), NULL, NULL, &status);

     char buf[256]; 

     sprintf(buf,"-cl-std=CL2.0 -w -I. -D T=%s -D faceNo=%d -D nodePerCell=%d -D polynomialOrder=%d -D omega=%f", 
            typeOfData, faceNo, this->nodePerCell, this->polynomialOrder, omega);

     buildOptions += std::string(buf);
  }
 
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Building program";

  try
  {
     program.build(buildOptions.c_str());
     std::cout << std::right << "  -> Done! " << std::endl;
  }catch(const cl::Error& error)
  {
     std::cout << "  -> Multigrid Class, Problem in building program " << std::endl;
     std::cout << "  -> " << getErrorString(error) << std::endl;
     std::cout << "  -> " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) 
              << std::endl;
     exit(0);
  }
}


/*
 * domainWidth -> number of cell in y direction
 * domainHeight -> number of cell in x direction
 * domainDepth -> number of cell in z direction
 */

template <class T>
void multiGrid<T>::setDomainSize()
{
  bufferOrigin[0] = 0;
  bufferOrigin[1] = 0;
  bufferOrigin[2] = 0;

  hostOrigin[0] = 0;
  hostOrigin[1] = 0;
  hostOrigin[2] = 0;
  
  cl::size_t<3> tmpRegion;
  cl::size_t<3> tmpCellNo;
 

  for (int i = 0; i <= multiGridLevels; i++)
  {
     spatialStepSize.push_back(this->origSpatialStepSize * pow(2,i));

     tmpCellNo[0] = (this->origCellNo_x - 2) / std::pow(2,i) + 2;
     tmpCellNo[1] = (this->origCellNo_y - 2) / std::pow(2,i) + 2;
     tmpCellNo[2] = (this->origCellNo_z - 2) / std::pow(2,i) + 2;
     domainCellNo.push_back(tmpCellNo);
     
     domainWidth.push_back(tmpCellNo[0]  * this->nodePerCellPerDir);
     domainHeight.push_back(tmpCellNo[1] * this->nodePerCellPerDir);
     domainDepth.push_back(tmpCellNo[2]  * this->nodePerCellPerDir);

     deviceWidth.push_back(roundUp(domainWidth[i] , WGX));
     deviceHeight.push_back(domainHeight[i]);
     deviceDepth.push_back(domainDepth[i]);
    
     deviceDataSize.push_back(deviceWidth[i] * deviceHeight[i] * 
                              deviceDepth[i] * sizeof(T));

     tmpRegion[0] = (size_t)(deviceWidth[i] * sizeof(T));
     tmpRegion[1] = (size_t)(domainHeight[i]);
     tmpRegion[2] = (size_t)(domainDepth[i]);
     region.push_back(tmpRegion);
  }
}


template <class T>
void multiGrid<T>::createBuffers()
{  
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Creation of Buffers" << std::endl;

  // local matrix has k^3 * k^3, where k is node Per cell in each dir
  stencilSize = this->nodePerCell * this->nodePerCell;

  int stencilDataSize   = stencilSize * sizeof(T);

  xBuffer               = cl::Buffer(context, CL_MEM_READ_WRITE,  deviceDataSize[0]);
  bBuffer               = cl::Buffer(context, CL_MEM_READ_WRITE,  deviceDataSize[0]);
  
  std::cout << std::setfill(' ') << std::setw(30);
  std::cout << std::right << "   -> Creation of Stiffness matrix" << std::endl;

  std::vector<cl::Buffer> M11Buf;
  std::vector<cl::Buffer> M12Buf;
  std::vector<cl::Buffer> M21Buf;
  std::vector<cl::Buffer> M22Buf;
 
  for (int i = 0; i <= multiGridLevels; i++)
  {
         
     std::cout << std::setfill(' ') << std::setw(20);
     std::cout << std::right << "       -> Level: " << i;
     volumeIntegralMatrixBuffer.push_back(cl::Buffer(context, CL_MEM_READ_ONLY, 
                                          stencilDataSize));

     for (int j = 0; j < faceNo; ++j)
     {
        M11Buf.push_back(cl::Buffer(context, CL_MEM_READ_ONLY, 
                               stencilDataSize));
        M12Buf.push_back(cl::Buffer(context, CL_MEM_READ_ONLY, 
                               stencilDataSize));
        M21Buf.push_back(cl::Buffer(context, CL_MEM_READ_ONLY, 
                               stencilDataSize));
        M22Buf.push_back(cl::Buffer(context, CL_MEM_READ_ONLY, 
                               stencilDataSize));
     }
     
     M11Buffer.push_back(M11Buf);
     M12Buffer.push_back(M12Buf);
     M21Buffer.push_back(M21Buf);
     M22Buffer.push_back(M22Buf);

     M11Buf.clear();
     M12Buf.clear();
     M21Buf.clear();
     M22Buf.clear();

     std::cout << std::left << "  -> Done! " << std::endl;
     residualBuffer.push_back(cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[i]));

     interResidualBuffer.push_back(cl::Buffer(context, CL_MEM_READ_WRITE, 
                                   deviceDataSize[i]));
    
     intermediateBuffer.push_back(cl::Buffer(context, CL_MEM_READ_WRITE, 
                                   deviceDataSize[i]));

     errorBuffer.push_back(cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[i]));
  }

  std::cout << std::setfill(' ') << std::setw(51);
  std::cout << std::right << "  -> Done! " << std::endl;
}


template <class T>
void multiGrid<T>::setNDRange()
{ 
  size_t maxGlobalWorkSize[3];
  cl::NDRange globalWorkSize; 
  std::vector<int> localSize;
  
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Setting Ranges!";
  
  for (int i = 0; i <= multiGridLevels; i++)
  {
     if (domainWidth[i] >= WGX && domainHeight[i] >= WGY && domainDepth[i] >= WGZ)
     {
        maxGlobalWorkSize[0] = 
                   (size_t)roundUp(domainWidth[i] - this->nodePerCellPerDir,  WGX);
        maxGlobalWorkSize[1] = 
                   (size_t)roundUp(domainHeight[i] - this->nodePerCellPerDir, WGY);
        maxGlobalWorkSize[2] = 
                   (size_t)roundUp(domainDepth[i] - this->nodePerCellPerDir,  WGZ);
        
        // global work size (total number of work items) equals
        //     (NUMBER OF CELL IN EACH DIR  - 1)* NUMBER OF NODE IN EACH DIR
        // in each dir
        globalWorkSize = cl::NDRange(maxGlobalWorkSize[0], maxGlobalWorkSize[1],  
                                     maxGlobalWorkSize[2]);
        
        globalRange.push_back(globalWorkSize);

        maxGlobalWorkSize[0] = 
               (size_t)roundUp(domainWidth[i]  - 2 * this->nodePerCellPerDir, WGX);
        maxGlobalWorkSize[1] = 
               (size_t)roundUp(domainHeight[i] - 2 * this->nodePerCellPerDir, WGY);
        maxGlobalWorkSize[2] = 
               (size_t)roundUp(domainDepth[i]  - 2 * this->nodePerCellPerDir, WGZ);
        
        // global work size (total number of work items) equals
        //     NUMBER OF CELL IN EACH DIR * NUMBER OF NODE IN EACH DIR
        // in each dir
        globalWorkSize = cl::NDRange(maxGlobalWorkSize[0], maxGlobalWorkSize[1],  
                                     maxGlobalWorkSize[2]);
        
        restProlGlobalRange.push_back(globalWorkSize);
        
        // local work size (number of item in a group)
        localRange.push_back(cl::NDRange((size_t) WGX, (size_t) WGY, (size_t) WGZ));

     } else if (domainWidth[i] < WGX || domainHeight[i] < WGX || domainDepth[i] < WGX)
     {
        maxGlobalWorkSize[0] = 
                (size_t)roundUp(domainWidth[i]  - 2 * this->nodePerCellPerDir, WGX);
        maxGlobalWorkSize[1] = 
                (size_t)roundUp(domainHeight[i] - 2 * this->nodePerCellPerDir, WGY);
        maxGlobalWorkSize[2] = 
                (size_t)roundUp(domainDepth[i]  - 2 * this->nodePerCellPerDir, WGZ);
        
        globalWorkSize = cl::NDRange(maxGlobalWorkSize[0], maxGlobalWorkSize[1],
                                     maxGlobalWorkSize[2]);
        restProlGlobalRange.push_back(globalWorkSize);

        maxGlobalWorkSize[0] = 
                   (size_t)roundUp(domainWidth[i] - this->nodePerCellPerDir, 1);
        maxGlobalWorkSize[1] = 
                   (size_t)roundUp(domainHeight[i] - this->nodePerCellPerDir, 1);
        maxGlobalWorkSize[2] = 
                   (size_t)roundUp(domainDepth[i] - this->nodePerCellPerDir, 1);
        
        globalWorkSize = cl::NDRange(maxGlobalWorkSize[0], maxGlobalWorkSize[1],  
                                     maxGlobalWorkSize[2]);
        
        globalRange.push_back(globalWorkSize);
        localRange.push_back(cl::NDRange((size_t) 1, (size_t) 1, (size_t) 1));
     }
  }

  std::cout << std::right << "  -> Done! " << std::endl;
}


template<class T>
void multiGrid<T>::fillZero(T* matrix)
{
  for (int i = 0; i < stencilSize; ++i)
  {
     matrix[i] = 0.0;
  }
}



template <class T>
void multiGrid<T>::writeDataToDevice()
{
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Writing Data to device";
  
  if (this->x == NULL || this->RHS == NULL)
  {
    std::cout << "  -> Multigrid Class, X or RHS is NULL"
              << std::endl;
    exit(0);
  }

  cl::size_t<3> dataRegion;
  dataRegion[0] = (size_t)((this->totalNodeNo_x) * sizeof(T));
  dataRegion[1] = (size_t)(this->totalNodeNo_y);
  dataRegion[2] = (size_t)(this->totalNodeNo_z);

  T* volumeIntegralMat = new T[stencilSize]();
  T* M11               = new T[stencilSize]();
  T* M12               = new T[stencilSize]();
  T* M21               = new T[stencilSize]();
  T* M22               = new T[stencilSize]();
  T* stiffMat          = new T[(faceNo + 1) * stencilSize]();
  
  try
  {
    queue.enqueueWriteBufferRect(xBuffer, CL_TRUE, bufferOrigin, hostOrigin, dataRegion,
            deviceWidth[0] * sizeof(T), 0, domainWidth[0] * sizeof(T), 0, this->x);

    queue.enqueueWriteBufferRect(bBuffer, CL_TRUE, bufferOrigin, hostOrigin, dataRegion,
           deviceWidth[0] * sizeof(T), 0, domainWidth[0] * sizeof(T), 0, this->RHS);

    int stencilDataSize = stencilSize * sizeof(T);
  
    for (int i = 0; i <= multiGridLevels; i++)
    { 
      this->dgLocalMatrix->createVolumeIntegral(volumeIntegralMat, spatialStepSize[i]);

      queue.enqueueWriteBuffer(volumeIntegralMatrixBuffer[i], CL_TRUE, 0,
                               stencilDataSize, volumeIntegralMat);

      // loop over faces
      for (int j = 0; j < faceNo; ++j)
      {
         this->dgLocalMatrix->createFaceIntegral(M11, M12, M21, M22,
                                                 spatialStepSize[i], j);

         queue.enqueueWriteBuffer(M11Buffer[i][j], CL_TRUE, 0,
                                  stencilDataSize, M11);
         queue.enqueueWriteBuffer(M12Buffer[i][j], CL_TRUE, 0,
                                  stencilDataSize, M12);
         queue.enqueueWriteBuffer(M21Buffer[i][j], CL_TRUE, 0,
                                  stencilDataSize, M21);
         queue.enqueueWriteBuffer(M22Buffer[i][j], CL_TRUE, 0,
                                  stencilDataSize, M22);

         fillZero(M11);
         fillZero(M12);
         fillZero(M21);
         fillZero(M22);
      }

      fillZero(volumeIntegralMat);
    }
      
  } catch (cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in writing data from Host to"
                   "  Device: " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  delete[] volumeIntegralMat;
  delete[] M11;
  delete[] M12;
  delete[] M21;
  delete[] M22;

  std::cout << std::right << "  -> Done! " << std::endl;
}



template<class T>
void multiGrid<T>::printStencil(T* stencilMatrix)
{
   for (int i = 0; i < this->nodePerCell; ++i)
   {
     for (int j = 0; j < this->nodePerCell; ++j)
     {
         std::cout << i << ",  " << stencilMatrix[i * this->nodePerCell + i] << "  " ;
     }
      std::cout << std::endl;
   }
   std::cout << std::endl;
   std::cout << std::endl;
}
      


template<class T>
void multiGrid<T>::copyBuffer(cl::Buffer& srcBuffer, cl::Buffer& distBuffer, 
                              int level)
{
  try
  {
    queue.enqueueCopyBufferRect(srcBuffer, distBuffer, bufferOrigin,
              hostOrigin, region[level], deviceWidth[level] * sizeof(T), 0,
              deviceWidth[level] * sizeof(T), 0, NULL, &event);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in copying buffers" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in finishing copyBufferRect" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }  
}


template<class T>
void multiGrid<T>::solve()
{}


template<class T>
void multiGrid<T>::readDataFromDevice()
{
  residual_h = new T[this->totalNodeNo_x * this->totalNodeNo_y * this->totalNodeNo_z]();
   
  cl::size_t<3> buffer_origin;
  cl::size_t<3> host_origin;
  cl::size_t<3> dataRegion;

  std::cout << "==> Start reading data from device" << std::endl;
  
  // read result y and residual from the device 
  buffer_origin[0] = 0;
  buffer_origin[1] = 0;
  buffer_origin[2] = 0;

  host_origin[0] = 0;
  host_origin[1] = 0;
  host_origin[2] = 0;

  // region of x
  dataRegion[0] = (size_t)((this->totalNodeNo_y) * sizeof(T));
  dataRegion[1] = (size_t)(this->totalNodeNo_x);
  dataRegion[2] = (size_t)(this->totalNodeNo_z);

  size_t buffer_row_pitch = deviceWidth[0] * deviceHeight[0] * sizeof(T);
  size_t host_row_pitch   = domainWidth[0] * domainHeight[0] * sizeof(T);
 
  try
  {
    queue.enqueueReadBufferRect(residualBuffer[0], CL_TRUE, buffer_origin, 
              host_origin, dataRegion, deviceWidth[0] * sizeof(T), buffer_row_pitch, 
              this->totalNodeNo_x * sizeof(T), host_row_pitch, residual_h);
    
    queue.enqueueReadBufferRect(xBuffer, CL_TRUE, buffer_origin, 
              host_origin, dataRegion, deviceWidth[0] * sizeof(T), buffer_row_pitch, 
              this->totalNodeNo_x * sizeof(T), host_row_pitch, this->x);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem reading buffer in device: "  << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in finishing reading buffer" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
} 


template<class T>
double multiGrid<T>::printData(cl::Buffer& buffer, int stepNumber, bool print, bool norm)
{
  cl::size_t<3> buffer_origin;
  cl::size_t<3> host_origin;
  cl::size_t<3> dataRegion;

  // read result y and residual from the device 
  buffer_origin[0] = 0;
  buffer_origin[1] = 0;
  buffer_origin[2] = 0;

  host_origin[0] = 0;
  host_origin[1] = 0;
  host_origin[2] = 0;

  dataRegion[0] = (size_t)((domainWidth[stepNumber]) * sizeof(T));
  dataRegion[1] = (size_t)(domainHeight[stepNumber]);
  dataRegion[2] = (size_t)(domainDepth[stepNumber]);

  int buffer_row_pitch = deviceWidth[stepNumber] * deviceHeight[stepNumber] * sizeof(T);
  int host_row_pitch   = domainWidth[stepNumber] * domainHeight[stepNumber] * sizeof(T);
 
  T* data = new T[domainWidth[stepNumber] * domainHeight[stepNumber] * 
                   domainDepth[stepNumber]]();
  try
  {
    queue.enqueueReadBufferRect(buffer, CL_TRUE, buffer_origin, 
              host_origin, dataRegion, deviceWidth[stepNumber] * sizeof(T), 
              buffer_row_pitch, domainWidth[stepNumber] * sizeof(T), host_row_pitch, 
              data);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem reading buffer in device: "  << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  double  residualNorm = this->norm_L2(data, 
            domainWidth[stepNumber] * domainHeight[stepNumber] * domainDepth[stepNumber], 
            spatialStepSize[stepNumber]);

  if (norm) std::cout << "   ->||r||_L2 = "  << residualNorm << std::endl;
  
  #ifdef DEBUG
  if (stepNumber != 0)
      mgGnuplot->plot(data, domainHeight[stepNumber], domainWidth[stepNumber], 
                        domainDepth[stepNumber], "error", stepNumber);
  #endif

   if (print)
   {
      std::cout << std::setprecision(4);
      for (int k = 0; k < this->origCellNo_z-1; ++k)
      {
        for (int i = 0; i < this->origCellNo_x-1; ++i)
        {
           for (int j = 0; j < this->origCellNo_y-1; ++j)
           {
             for (int in = 0; in < this->nodePerCell; ++in)
             {
                int arrayNumber = (k * this->origCellNo_x * this->origCellNo_y +
                                  i * this->origCellNo_y + j) * this->nodePerCell + in;

                std::cout << *(data + arrayNumber) << "  ";
             }
             std::cout << "\n";
           }
           std::cout << "\n";
           std::cout << "\n";
        }
        std::cout << "\n\n";
        std::cout << "\n";
      }
   }

   return residualNorm;
}


template<class T>
inline void multiGrid<T>::printSettingData(std::string multigridAlg)
{
  std::cout << "\n_______________________SETUP________________________\n" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Method";
  std::cout << std::right << ":  " << multigridAlg << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Size of Domain (m)"; 
  std::cout << std::right << ":  " << this->spatialLength << " * " << 
                this->spatialLength << " * " << this->spatialLength << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Spatial Steps" ;
  std::cout << std::right  << ":  " << this->origCellNo_x - 2 << " * " <<
               this->origCellNo_y - 2 << " * " << this->origCellNo_z - 2 << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Polynomial Order" ;
  std::cout << std::right  << ":  " << this->polynomialOrder << std::endl;
  
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"dG Method" ;
  std::cout << std::right  << ":  " << this->dgLocalMatrix->getMethod() << std::endl;
  
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Sigma" ;
  std::cout << std::right  << ":  " << this->dgLocalMatrix->getSigma() << std::endl;
  
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Penalty" ;
  std::cout << std::right  << ":  " << this->dgLocalMatrix->getPenalty() << std::endl;
  
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Total Node Number" ;
  std::cout << std::right  << ":  " << this->totalNodeNo_x << " * " <<
               this->totalNodeNo_y << " * " << this->totalNodeNo_z  << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Global Range";
  std::cout << std::right << ":  " << globalRange[0][0] << " * " 
            << globalRange[0][1] << " * " << globalRange[0][2] << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Local Range";
  std::cout << std::right << ":  " << localRange[0][0] << " * " 
            << localRange[0][1] << " * " << localRange[0][2] << std::endl;

}


template<class T>
void multiGrid<T>::printResult(long double exeTime, double normResidualRatio, 
                               double normErrorRatio, std::string multigridAlg,
                               std::string relaxationMethod, T omega,
                               int nu_1, int nu_2, int numberOfSteps)
{
  int domainSize = this->domainHeight[0] * this->domainWidth[0] * this->domainDepth[0];
  double WU = 0.0;  

  int operNo = this->nodePerCell * 7 * 2 - 1;
  if (multigridAlg == "V-Cycle")
  {
     for (int i = 0; i < multiGridLevels; ++i)
     {
        WU += 1.0 / pow(2,i);
     }
     WU *=domainSize * operNo * (nu_1 + nu_2 + 2) * numberOfSteps;
  } else if (multigridAlg == "Full Multigrid")
  {
     for (int i = 0; i < multiGridLevels; ++i)
     {
        WU += 1.0 / pow(2,i);
     }
     WU *= 2.0 / (1.0 - pow(2,-3)) * domainSize * operNo * 
           (nu_1 + nu_2 + 2) * numberOfSteps;
  }

  printSettingData(multigridAlg);

  std::cout << "\n_______________________RESULT________________________\n" << std::endl;
  
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Multigrid Steps";
  std::cout << std::right << ":  " << numberOfSteps << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Multigrid Level";
  std::cout << std::right << ":  " << multiGridLevels + 1 << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Smotthing Scheme";
  std::cout << std::right << ":  " << relaxationMethod << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Omega";
  std::cout << std::right << ":  " <<  omega << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Pre-Smoothing Sweep";
  std::cout << std::right << ":  " << nu_1 << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Post-Smoothing Sweep";
  std::cout << std::right << ":  " <<  nu_2 << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Ratio ||e||_L2"; 
  std::cout << std::right  << ":  " << pow(normErrorRatio, 1.0/numberOfSteps) 
               << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Ratio ||r||_L2"; 
  std::cout << std::right  << ":  " <<  pow(normResidualRatio, 1.0/numberOfSteps) 
               << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Exe. time";
  std::cout << std::right  << ":  " << exeTime << " u sec" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Performance"; 
  std::cout << std::right  << ":  " << WU/exeTime/1.0e3 << " GFlops/sec" << std::endl;

  std::cout << "_____________________________________________________\n" << std::endl;
}

