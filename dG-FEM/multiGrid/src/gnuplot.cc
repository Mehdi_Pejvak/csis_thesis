
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 05.05.2020

Note:
    Member function "plot" plots given "vector" using "gnuplot", as well as write 
    "vector" into ".cvs" file with header to be loaded into data visualisation
    software like "paraview". To use data in paraview, load data and use "Table to Point"
    filter.
\*-----------------------------------------------------------------------*/



#include "../includes/gnuplot.hpp"

template<class T>
gnuplot<T>::gnuplot()
{
  std::string gnuplot;
}


template<class T>
gnuplot<T>::~gnuplot()
{

}




template<class T>
void gnuplot<T>::plot( const T* vector, const int& height, const int& width, const int& depth, std::string nameOfImage, const int& stepNumber)
{
   std::string paraview = "./data/paraview.csv." + std::to_string(count);
   std::ofstream paraviewStream(paraview);

   count++;   
   int arrayNumber;

   double spatialSZ = 1.0 / width;  

   std::string fileName = "dataFile";
   std::ofstream fileStream(fileName);
 
   // Header for .csv file to load in paraview
   paraviewStream << "X, Y, Z, " << nameOfImage << std::endl;

   for (int k = 0; k < depth; ++k)
   {
      for (int i = 0; i < height; ++i)
      {
          for (int j = 0; j < width; ++j)
          {
            arrayNumber = k * height * width + i * width + j;

            paraviewStream << j * spatialSZ << ", " << i * spatialSZ << ", " 
                           << k * spatialSZ << ", " << *(vector + arrayNumber) << "\n" ;
            if (gnuPlotting)
            {
               fileStream << j << " " << i +  k * depth << " " << 
                         *(vector + arrayNumber) << "\n" ;
            }
          }
      }
   }

  if (gnuPlotting)
  {
    std::ofstream file("file");
    std::ostringstream gnuplot;


    gnuplot << "set output './data/" << nameOfImage << "_" << stepNumber << ".png'\n";
    gnuplot << "set terminal png\n";
    gnuplot << "set title \"" << nameOfImage << "_" << count << " in level " << 
               stepNumber <<  " ,depth = "  << depth <<   "\"\n";
/* // 4D plot

    gnuplot << "set xlabel \"width  \" \n";
    gnuplot << "set ylabel \"height \" \n";
    gnuplot << "set zlabel \"depth  \" \n";
    gnuplot << "set terminal png size 1024,768\n";

    gnuplot << "set dgrid3d "  <<  width << "," << floor(sqrt(width)) << "\n";
    gnuplot << "dataFile='" << fileName << "'\n";

    gnuplot << "set table dataFile.'.grid'\n";
    gnuplot << "splot dataFile u 1:2:3\n";
    gnuplot << "unset table\n";

    gnuplot << "set table dataFile.'.color'\n";
    gnuplot << "splot dataFile u 1:2:4\n";
    gnuplot << "unset table\n";

    gnuplot << "set view 60,45\n";
    gnuplot << "set hidden3d\n";
    gnuplot << "set autoscale cbfix\n";
    gnuplot << "set pm3d\n";
    gnuplot << "unset dgrid3d\n";
    gnuplot << "set ticslevel 0\n";
    gnuplot << "splot sprintf('< paste %s.grid %s.color', dataFile, dataFile) u 1:2:3:7 with pm3d notitle\n";
*/
/*
 // coloured circle
    gnuplot << "set xlabel \"width  \" \n";
    gnuplot << "set ylabel \"height \" \n";
    gnuplot << "set zlabel \"depth  \" \n";
    gnuplot << "set terminal png size 1024,768\n";
    gnuplot << "set palette rgb 33,13,10\n";
    gnuplot << "splot \"" << fileName << "\" u 1:2:3:4 w p pt 7 ps 2 palette notitle\n";
    gnuplot << "set terminal x11\n";
    gnuplot << "set output\n";
    gnuplot << "replot\n";
*/

 // 3D with lines
    gnuplot << "set dgrid3d "  <<  width << "," << floor(sqrt(width)) << "\n";
    gnuplot << "dataFile='" << fileName << "'\n";
    gnuplot << "set contour\n";
  // gnuplot << "set pm3d at b\n";
    gnuplot << "set hidden3d\n";
    gnuplot << "set xlabel \"width\" \n";
    gnuplot << "set ylabel \"height * depth\" \n";
    gnuplot << "set zlabel \"" << nameOfImage << "\" \n";
    gnuplot << "splot \"" << fileName  <<"\" using 1:2:3 with lines\n";
    gnuplot << "set terminal x11\n";
    gnuplot << "set output\n";
    gnuplot << "replot\n";

    std::string cmdStr = gnuplot.str();
    file << cmdStr;
    file.close();
    system("gnuplot --persist file");
    system("rm -f file");
  } 
  system("rm -f dataFile");
}
