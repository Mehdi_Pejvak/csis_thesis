
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 05.05.2020
\*-----------------------------------------------------------------------*/


#ifndef GNUPLOT_HPP
#define GNUPLOT_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>

template <class T>
class gnuplot
{
  public:
    gnuplot();

    ~gnuplot();

    void plot(const T* vector, const int& height, 
              const int& width, const int& depth,
              std::string nameOfImage, const int& stepNumber); 
  private:
    int count = 0;
    bool gnuPlotting = false;
};

#endif
