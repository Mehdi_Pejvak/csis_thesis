/*-----------------------------------------------------------------------*\
  	      __________________________________________________
	     /                                                 /|
	    /_________________________________________________/ |
	   |                                                 |  |
	   |     __  _______     _______    _____  __   __   |  |
	   |    |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |    |  |   /       |   /       | |_)  |  | |  |  |  |
	   |  __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   | /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   ||  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   | \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                 |  |
           |    discontinuous Galerkin    On      GPGPU      | /
	   |_________________________________________________|/

 --------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 03.05.2020
\*-----------------------------------------------------------------------*/

#ifndef MULTIGRID_HPP
#define MULTIGRID_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

//#define DEBUG

#include <iostream>
#include <cassert>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>
#include <cmath>
#include <iomanip>
#include <chrono>
#include <cstdlib>
#include <cxxabi.h>
#include <typeinfo>

#include "CL_ERROR.hpp"
#include "defines.hpp"
#include "solver.hpp"
#include "residual.hpp"
#include "relaxation.hpp"
#include "prolongation.hpp"
#include "restriction.hpp"
#include "gnuplot.hpp"
#include "../../dG/includes/localStiffMatrix.hpp"


template<class T>
class restriction;

template<class T>
class gnuplot;

template<class T>
class relaxation;

template<class T>
class residual;

template<class T>
class prolonation;

template<class T>
class boundaryCondition;

template<class T>
class solver;

template<class T>
struct localStiffMatrix;


template<class T>
class multiGrid : public solver<T>
{
  protected:
    int deviceID = 0;
    int multiGridLevels = 0;
    T* residual_h = NULL;
    
    // number of cell in local memory for relax & residual
    const int cellInLocalMem = 4;

    // number of cell in local memory for prolongation and restriction
    const int intplCellLocMem = 5;

    // number of face in 3D element
    const int faceNo = 6;

    // size of stiffness local matrix
    int stencilSize;
    
    std::string kernelFileName = 
                   "/home/mehdi/csis_thesis/dG-FEM/multiGrid/kernels/multigrid.cl";
    
    cl_device_type deviceType = CL_DEVICE_TYPE_GPU;

    std::string relaxationMethod = "jacobi";
    T omega = 0.857;
 
    std::vector<cl::Platform> platforms;
    cl::Platform myPlatform;
    std::vector<cl::Device> devices;

    cl::Device device;
    cl::CommandQueue queue;
    cl::Context context;
    cl::Program program;

    std::vector<int> domainWidth;
    std::vector<int> domainHeight;
    std::vector<int> domainDepth;
    std::vector<T> spatialStepSize;

    cl::Buffer xBuffer;
    cl::Buffer relaxedXBuffer;
    cl::Buffer bBuffer;
   
    std::vector<cl::Buffer> volumeIntegralMatrixBuffer; 
    std::vector<std::vector<cl::Buffer>> M11Buffer; 
    std::vector<std::vector<cl::Buffer>> M12Buffer; 
    std::vector<std::vector<cl::Buffer>> M21Buffer; 
    std::vector<std::vector<cl::Buffer>> M22Buffer; 
    
    std::vector<cl::Buffer> residualBuffer;          // r = b - Ax 
    std::vector<cl::Buffer> interResidualBuffer;     // r_2h = I_h^2h(r_h) 
    std::vector<cl::Buffer> intermediateBuffer;      // use for intermediate steps
    std::vector<cl::Buffer> errorBuffer;             // e = |x* - x|
   
    std::vector<localStiffMatrix<T>> mgLocalStiffMatrix;

    cl::Event event;
    
    std::vector<int> deviceWidth;
    std::vector<int> deviceHeight;
    std::vector<int> deviceDepth;
    std::vector<int> deviceDataSize;
    std::vector<cl::size_t<3>> domainCellNo;

    cl::size_t<3> bufferOrigin;
    cl::size_t<3> hostOrigin;
    std::vector<cl::size_t<3>> region;

    std::vector<cl::NDRange> globalRange;
    std::vector<cl::NDRange> restProlGlobalRange;
    std::vector<cl::NDRange> localRange;
 
    residual<T>* mgResidual;
    relaxation<T>* mgRelaxation;
    restriction<T>* mgRestriction;
    prolongation<T>* mgProlongation;
    gnuplot<T>* mgGnuplot;
    
  public:
    multiGrid(T* _RHS,
           int _origHeight, int _origWidth, int _origDepth,
           std::string _relaxationMethod, T _omega,   
           cl_device_type _deviceType, int _deviceID,
           int _polynomialOrder, std::string _method, double _penalty);

    multiGrid(std::ifstream _RHSFileName,
           int _origHeight, int _origWidth, int _origDepth, 
           std::string _relaxationMethod, T _omega,   
           cl_device_type _deviceType, int _deviceID,
           int _polynomialOrder, std::string _method, double _penalty);
 
    multiGrid(int _origHeight, int _origWidth, int _origDepth,
           std::string _relaxationMethod, T _omega,   
           cl_device_type _deviceType, int _deviceID,
           int _polynomialOrder, std::string _method, double _penalty);
    
    multiGrid(solver<T>& _mgSolver, 
           std::string _relaxationMethod, T _omega,   
           cl_device_type _deviceType, int _deviceID,
           int _polynomialOrder, std::string _method, double _penalty);

    multiGrid(std::string _relaxationMethod, T _omega, 
              cl_device_type _deviceType, int _deviceID);

    ~multiGrid();

    void setDevice();

    void displayDeviceInfo();    

    int roundUp (unsigned int value, unsigned int multiple);    

    void setNumberOfMGLevels();

    void buildProgram(T omega);

    void setDomainSize();

    void createBuffers();

    void setNDRange();

    void fillZero(T* matrix);

    void writeDataToDevice();

    void printStencil(T* stencilMatrix);

    virtual void solve() override;

    void copyBuffer(cl::Buffer& srcBuffer, cl::Buffer& distBuffer, int level);

    void readDataFromDevice();

    double printData(cl::Buffer& buffer, int stepNumber, bool print, bool norm);

    void printSettingData(std::string multigridAlg);

    void printResult(long double exeTime, double normResidualRatio, 
                     double normErrorRation, std::string multigridAlg, 
                     std::string relaxationMethod, T omega, 
                     int nu_1, int nu_2, int numberOfSteps);
};


#endif
