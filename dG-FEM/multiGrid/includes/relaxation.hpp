/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Sat 11.04.2020
\*-----------------------------------------------------------------------*/

#ifndef RELAXATION_HPP
#define RELAXATION_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include "CL_ERROR.hpp"
#include "defines.hpp"
#include <chrono>
#include <cmath>

template<class T>
class relaxation
{
  protected:
    std::string relaxationMethod;
    T omega = 1.0;  
    
    const int faceNo = 6;
    const int cellInLocalMem = 4;

    long double timeOfOperation = 0.0;

    cl::Kernel       kernelJacobi;
    cl::Kernel       kernelGSRed;
    cl::Kernel       kernelGSBlack;
    cl::Kernel       kernelDirectSolver;

  public:
     relaxation(cl::Program& _programInput, cl::CommandQueue& _queue, 
                const cl::Context& _context, std::string relaxationMethod_, T omega);

    ~relaxation();

     const T getOmega() const;
    
     const long double getTime() const;
  
     void createKernel(cl::Program& program);

     void relaxing(cl::CommandQueue& queue, 
                   cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer, 
                   cl::Buffer& intermediateBUffer,
                   const int polynomialOrder,
                   const cl::Buffer& volumeIntegralMatrixBuffer,
                   const std::vector<cl::Buffer> M11Buffer,
                   const std::vector<cl::Buffer> M12Buffer,
                   const std::vector<cl::Buffer> M21Buffer,
                   const std::vector<cl::Buffer> M22Buffer,
                   const cl::NDRange& globalRange, 
                   const cl::NDRange& localRange,
                   const int numberOfRelaxationSweep,
                   const cl::size_t<3> domainCellNo, 
                   cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                   cl::size_t<3> region,
                   cl::Event& event);


   void jacobi(cl::CommandQueue& queue, 
                   cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer, 
                   cl::Buffer& intermediateBUffer,
                   const int polynomialOrder,
                   const cl::Buffer& volumeIntegralMatrixBuffer,
                   const std::vector<cl::Buffer> M11Buffer,
                   const std::vector<cl::Buffer> M12Buffer,
                   const std::vector<cl::Buffer> M21Buffer,
                   const std::vector<cl::Buffer> M22Buffer,
                   const cl::NDRange& globalRange, 
                   const cl::NDRange& localRange,
                   const int numberOfRelaxationSweep,
                   const cl::size_t<3> domainCellNo, 
                   cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                   cl::size_t<3> region,
                   cl::Event& event);

   void Guass_Seidel(cl::CommandQueue& queue, 
                   cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer, 
                   cl::Buffer& intermediateBUffer,
                   const int polynomialOrder,
                   const cl::Buffer& volumeIntegralMatrixBuffer,
                   const std::vector<cl::Buffer> M11Buffer,
                   const std::vector<cl::Buffer> M12Buffer,
                   const std::vector<cl::Buffer> M21Buffer,
                   const std::vector<cl::Buffer> M22Buffer,
                   const cl::NDRange& globalRange, 
                   const cl::NDRange& localRange,
                   const int numberOfRelaxationSweep,
                   const cl::size_t<3> domainCellNo, 
                   cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                   cl::size_t<3> region,
                   cl::Event& event);

    void directSolver(cl::CommandQueue& queue,
                   cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                   cl::Buffer& intermediateBuffer,
                   const int polynomialOrder,
                   const cl::Buffer& volumeIntegralMatrixBuffer,
                   const std::vector<cl::Buffer> M11Buffer,
                   const std::vector<cl::Buffer> M12Buffer,
                   const std::vector<cl::Buffer> M21Buffer,
                   const std::vector<cl::Buffer> M22Buffer,
                   const cl::NDRange& globalRange, 
                   const cl::NDRange& localRange,
                   const cl::size_t<3> domainCellNo, 
                   cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                   cl::size_t<3> region, 
                   cl::Event& event);


};

#endif
