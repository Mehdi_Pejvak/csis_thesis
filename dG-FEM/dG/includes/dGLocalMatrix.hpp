

/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 28.08.2020
\*---------------------------------------------------------------------------*/

#ifndef DGLOCALMATRIX_HPP
#define DGLOCALMATRIX_HPP

#include "/home/mehdi/library/pybind11/include/pybind11/embed.h"
#include "/home/mehdi/library/pybind11/include/pybind11/numpy.h"


#pragma GCC visibility push(hidden)

namespace py = pybind11;


template <class T>
class dGLocalMatrix
{
   private:
     const char* moduleName = "../python/dG";
     const char* className = "dg_local_matrix";
     const char* methodRHS = "create_right_hand_side";
     const char* methodVolumeIntegral = "create_volume_integral";
     const char* methodFaceIntegral   = "create_face_integral";
     const char* methodFaceM11        = "create_face_M11";

     const double quadPoint[4] = {-1.0, -sqrt(5)/5.0, sqrt(5.0)/5.0, 1.0};
     const double weight[4] = {1.0/6.0, 5.0/6.0, 5.0/6.0, 1.0/6.0};

     double sigma = 0.0;
     double penalty = 0.0;
     int polynomialOrder = 1.0;

   public:
     dGLocalMatrix(int _polynomialOrder, double _sigma, double _penalty);
     
     ~dGLocalMatrix();
     
     void createVolumeIntegral(const int dim, T* stencil);
     
     void createFaceIntegral(const int dim, T* stencil);
     
     void createFaceM11Integral(const int dim, T* stencil);
     
     void createRightHandSide(const int dim, T* stencil);

};

#pragma GCC visibility pop
#endif

